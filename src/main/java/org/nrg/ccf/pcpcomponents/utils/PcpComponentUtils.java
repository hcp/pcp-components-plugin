package org.nrg.ccf.pcpcomponents.utils;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.nrg.xdat.XDAT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * The Class PcpComponentUtils.
 */
public class PcpComponentUtils {
	
	/** The Constant _logger. */
	private static final Logger _logger = LoggerFactory.getLogger(PcpComponentUtils.class);
	
	/** The Constant _gson. */
	protected static final Gson _gson = new Gson();
	
	/** The config service. */
	protected static ConfigService _configService = null;
	
	/**
	 * Gets the config value.
	 *
	 * @param project the project
	 * @param pipeline the pipeline
	 * @param className the class name
	 * @param keyContains the key contains
	 * @param throwException the throw exception
	 * @return the config value
	 * @throws PcpAutomationScriptExecutionException the pcp automation script execution exception
	 */
	public static Object getConfigValue(String project, String pipeline, String className, String keyContains, boolean throwException) throws PcpAutomationScriptExecutionException {
		final List<PcpConfigInfo> configInfoList = getPcpConfigInfoList(project);
		for (final PcpConfigInfo configInfo : configInfoList) {
			if (configInfo.getPipeline().equals(pipeline)) {
				final Map<String, Object> configurables = configInfo.getConfigurables();
				for (final String key : configurables.keySet()) {
					if (key.startsWith(className.replace(".", "") + "-") && key.contains(keyContains)) {
						return configurables.get(key);
					}
				}
				if (throwException) {
					throw new PcpAutomationScriptExecutionException("Could not retrieve PCP configuration for this project pipeline component "
							+ "(PROJECT=" + project + ", PIPELINE=" + pipeline + ", COMPONENT=" + className + ")");
					
				} else {
					_logger.warn("Could not retrieve PCP create-workflow configuration for this project pipeline component.  Maybe it's just empty. "
						+ "(PROJECT=" + project + ", PIPELINE=" + pipeline + ", COMPONENT=" + className + ")");
					return "";
					
				}
			}
		}
		if (throwException) {
			throw new PcpAutomationScriptExecutionException("Could not retrieve PCP configuration for this project pipeline component "
					+ "(PROJECT=" + project + ", PIPELINE=" + pipeline + ", COMPONENT=" + className + ")");
			
		} else {
			_logger.warn("Could not retrieve PCP configuration for project pipeline. Maybe it's just empty. " +
					"(PROJECT=" + project + ", PIPELINE=" + pipeline +")");
			return "";
		}
	}

	/**
	 * Gets the pcp config info list.
	 *
	 * @param project the project
	 * @return the pcp config info list
	 * @throws PcpAutomationScriptExecutionException the pcp automation script execution exception
	 */
	public static List<PcpConfigInfo> getPcpConfigInfoList(String project)
			throws PcpAutomationScriptExecutionException {
		final Configuration conf = getConfigService().getConfig(PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH,
				Scope.Project, project);
		final List<PcpConfigInfo> configInfoList = _gson.fromJson(conf.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
		if (configInfoList == null || configInfoList.size()<1) {
			throw new PcpAutomationScriptExecutionException("Could not retrieve PCP configuration for project (" + project + ")");
		}
		return configInfoList;
	}

	/**
	 * Gets the config service.
	 *
	 * @return the config service
	 */
	public static ConfigService getConfigService() {
		if (_configService  == null) {
			_configService = XDAT.getContextService().getBean(ConfigService.class);
		}
		return _configService;
	}
	
}
