package org.nrg.ccf.pcpcomponents.execmanagers;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.nrg.ccf.pcp.dto.PcpProjectPipeline;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcurrentExecManagerRunner implements Runnable {

	private static final Logger _logger = LoggerFactory.getLogger(ConcurrentExecManagerRunner.class);
	public static final ConcurrentMap<PcpProjectPipeline,MutableInt> cache = new ConcurrentHashMap<>();
	private PcpStatusEntityService _service;
	private PipelineSubmitterI submitter;
	private PipelineValidatorI validator;
	private PcpCondensedStatusI entity;
	private PcpStatusEntity statusEntity;
	private Map<String, String> parameters;
	private UserI user;

	public ConcurrentExecManagerRunner(PcpStatusEntityService _service, PipelineSubmitterI submitter,
			PipelineValidatorI validator, PcpCondensedStatusI entity, PcpStatusEntity statusEntity, Map<String, String> parameters, UserI user) {
		this._service = _service;
		this.submitter = submitter;
		this.validator = validator;
		this.entity = entity;
		this.statusEntity = statusEntity;
		this.parameters = parameters;
		this.user = user;
		
	}

	@Override
	public void run() {
		try {
			incrementCache();
			if (statusEntity == null) {
				return;
			}
			final Date submitTime = new Date();
			statusEntity.setSubmitTime(submitTime);
			_service.update(statusEntity);
			try {
				submitter.submitJob(statusEntity, _service, validator, parameters, user);
			} catch (Exception e) {
				_logger.error("Exception thrown processing  entity (" + entity + "):" + ExceptionUtils.getStackTrace(e));
			}
			// submit process may have updated the entity
			_service.refresh(statusEntity);
			validator.validate(statusEntity, user);
			_service.update(statusEntity);
		} finally {
			decrementCache();
		}
	}

	private synchronized void decrementCache() {
		final PcpProjectPipeline projectPipeline = new PcpProjectPipeline(statusEntity.getProject(), statusEntity.getPipeline());
		initializeCache(projectPipeline);
		MutableInt currentlyRunning = cache.get(projectPipeline);
		currentlyRunning.decrement();
		_logger.debug("ConcurrentExecManagerRunner - decremented - currentlyRunning - " + currentlyRunning +
				"   statusEntity - " + statusEntity);
		
	}

	private synchronized void incrementCache() {
		final PcpProjectPipeline projectPipeline = new PcpProjectPipeline(statusEntity.getProject(), statusEntity.getPipeline());
		initializeCache(projectPipeline);
		MutableInt currentlyRunning = cache.get(projectPipeline);
		currentlyRunning.increment();
		_logger.debug("ConcurrentExecManagerRunner - incremented - currentlyRunning - " + currentlyRunning +
				"   statusEntity - " + statusEntity);
		
	}

	public static boolean canRunJob(PcpStatusEntity statusEntity, int max_jobs) {
		final PcpProjectPipeline projectPipeline = new PcpProjectPipeline(statusEntity.getProject(), statusEntity.getPipeline());
		initializeCache(projectPipeline);
		MutableInt currentlyRunning = cache.get(projectPipeline);
		_logger.debug("ConcurrentExecManagerRunner - canRunJob - currentlyRunning - " + currentlyRunning +
				"   max_jobs - " + max_jobs + "   statusEntity - " + statusEntity);
		return currentlyRunning.intValue()<max_jobs;
	}

	private static void initializeCache(PcpProjectPipeline projectPipeline) {
		if (!cache.containsKey(projectPipeline)) {
			cache.put(projectPipeline, new MutableInt(0));
		}
	}

}
