package org.nrg.ccf.pcpcomponents.execmanagers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.nrg.ccf.pcp.abst.PipelineExecManagerA;
import org.nrg.ccf.pcp.anno.PipelineExecManager;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpExecService;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelineExecManager(resetStatusOnStartup = true)
public class ConcurrentExecManager extends PipelineExecManagerA implements ConfigurableComponentI {
	
	private static final Logger _logger = LoggerFactory.getLogger(ConcurrentExecManager.class);
	// TODO:  Make this configurable for each project/pipeline
	private static int DEFAULT_MAX_JOBS = 5;
	protected final String _className = this.getClass().getName().replace(".", "");
	private Integer configuredMaxJobs = null;

	@Override
	public List<String> getConfigurationYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"maxJobs:\n" +
				"    id: " + _className + "-max-jobs\n" +
				"    name: " + _className + "-max-jobs\n" +
				"    kind: panel.select.single\n" +
				"    label: 'Max Concurrent Jobs'\n" +
				"    value: '5'\n" +
				"    options:\n" +
				"        '1': '1'\n" +
				"        '2': '2'\n" +
				"        '3': '3'\n" +
				"        '4': '4'\n" +
				"        '5': '5'\n" +
				"        '6': '6'\n" +
				"        '7': '7'\n" +
				"        '8': '8'\n" +
				"        '9': '9'\n" +
				"        '10': '10'\n" +
				"        '11': '11'\n" +
				"        '12': '12'\n" +
				"        '13': '13'\n" +
				"        '14': '14'\n" +
				"        '15': '15'\n" +
				"        '16': '16'\n";
		returnList.add(ele);
		return returnList;
	}
	
	protected Integer getMaxJobs(String project, String pipeline) {
		if (configuredMaxJobs==null) {
			try {
				final String configValue = PcpComponentUtils.getConfigValue(project, pipeline, 
					this.getClass().getName(), "-max-jobs", false).toString();
				configuredMaxJobs = (configValue!=null) ? Integer.valueOf(configValue) : DEFAULT_MAX_JOBS;
			} catch (Exception e) {
				configuredMaxJobs = DEFAULT_MAX_JOBS;
			}
		}
		_logger.debug("ConcurrentExecManager - configuredMaxJobs - " + configuredMaxJobs);
		return configuredMaxJobs;
	}
	
	@Override
	public void doSubmit(PipelineSubmitterI submitter, PipelineValidatorI validator, List<PcpCondensedStatusI> statusList, Map<String,String> parameters, UserI user) {
		
		final PcpStatusEntityService _service = XDAT.getContextService().getBean(PcpStatusEntityService.class);
		Integer maxJobs = null;
		
		for (final PcpCondensedStatusI entity : statusList) {
			final PcpStatusEntity statusEntity = _service.getStatusEntity(entity);
			if (maxJobs == null) {
				maxJobs = getMaxJobs(statusEntity.getProject(),statusEntity.getPipeline());
			}
			while(!ConcurrentExecManagerRunner.canRunJob(statusEntity,maxJobs)) {
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// Do nothing
				}
			}
			_logger.debug("ConcurrentExecManager - Launch job - " + statusEntity);
			_logger.debug("Running as user=" + user.getLogin());
			final ConcurrentExecManagerRunner runner = new ConcurrentExecManagerRunner(_service,submitter,validator,entity,statusEntity,parameters,user);
			PcpExecService.execute(runner);
			try {
				// Sleep a bit to make sure running status has been updated
				TimeUnit.MILLISECONDS.sleep(500);
			} catch (InterruptedException e) {
				// Do nothing
			}
		}
	}

}
