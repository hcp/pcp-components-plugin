package org.nrg.ccf.pcpcomponents.execmanagers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.pcp.abst.PipelineExecManagerA;
import org.nrg.ccf.pcp.anno.PipelineExecManager;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelineExecManager(resetStatusOnStartup = true)
public class SimpleExecManager extends PipelineExecManagerA {
	
	private static final Logger _logger = LoggerFactory.getLogger(SimpleExecManager.class);

	@Override
	public void doSubmit(PipelineSubmitterI submitter, PipelineValidatorI validator, List<PcpCondensedStatusI> statusList, Map<String,String> parameters, UserI user) {
		
		final PcpStatusEntityService _service = XDAT.getContextService().getBean(PcpStatusEntityService.class);
		for (final PcpCondensedStatusI entity : statusList) {
			final PcpStatusEntity statusEntity = _service.getStatusEntity(entity);
			if (statusEntity == null) {
				continue;
			}
			final Date submitTime = new Date();
			statusEntity.setSubmitTime(submitTime);
			_service.update(statusEntity);
			try {
				submitter.submitJob(statusEntity, _service, validator, parameters, user);
			} catch (Exception e) {
				_logger.error("Exception thrown processing  entity (" + entity + "):" + ExceptionUtils.getStackTrace(e));
			}
			// submit process may have updated the entity
			_service.refresh(statusEntity);
			validator.validate(statusEntity, user);
			_service.update(statusEntity);
		}
	}

}
