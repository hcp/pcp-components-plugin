package org.nrg.ccf.pcpcomponents.abst;

import java.util.Date;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;

public abstract class AbstractPipelineValidator implements PipelineValidatorI {
	
	public void validationFailed(PcpStatusEntity entity, String validatedInfo) {
		if (entity.getValidated() == null || !validatedInfo.equals(entity.getValidatedInfo()) || entity.getValidated())  {
			entity.setValidated(false);
			entity.setValidatedInfo(validatedInfo);
			entity.setValidatedTime(new Date());
		}
	}
	
	public void validationFailed(PcpStatusEntity entity) {
		if (entity.getValidated() == null || !entity.getValidatedInfo().isEmpty() || entity.getValidated())  {
			entity.setValidated(false);
			entity.setValidatedInfo("");
			entity.setValidatedTime(new Date());
		}
	}
	
	public void validationPassed(PcpStatusEntity entity, String validatedInfo) {
		if (entity.getValidated() == null || !validatedInfo.equals(entity.getValidatedInfo()) || !entity.getValidated())  {
			entity.setValidated(true);
			entity.setValidatedInfo(validatedInfo);
			entity.setValidatedTime(new Date());
		}
	}
	
	public void validationPassed(PcpStatusEntity entity) {
		if (entity.getValidated() == null || !entity.getValidatedInfo().isEmpty() || !entity.getValidated())  {
			entity.setValidated(true);
			entity.setValidatedInfo("");
			entity.setValidatedTime(new Date());
		}
	}
	
	public void issuesRaised(PcpStatusEntity entity, String issuesInfo) {
		if (entity.getIssues() == null || !issuesInfo.equals(entity.getIssuesInfo()) || !entity.getIssues())  {
			entity.setIssues(true);
			entity.setIssuesInfo(issuesInfo);
			entity.setIssuesTime(new Date());
		}
	}
	
	public void issuesRaised(PcpStatusEntity entity) {
		if (entity.getValidated() == null || !entity.getIssuesInfo().isEmpty() || !entity.getIssues())  {
			entity.setIssues(true);
			entity.setIssuesInfo("");
			entity.setIssuesTime(new Date());
		}
	}
	
	public void noIssuesRaised(PcpStatusEntity entity, String issuesInfo) {
		if (entity.getIssues() == null || !issuesInfo.equals(entity.getIssuesInfo()) || entity.getIssues())  {
			entity.setIssues(false);
			entity.setIssuesInfo(issuesInfo);
			entity.setIssuesTime(new Date());
		}
	}
	
	public void noIssuesRaised(PcpStatusEntity entity) {
		if (entity.getIssues() == null || !entity.getIssuesInfo().isEmpty() || entity.getIssues())  {
			entity.setIssues(false);
			entity.setIssuesInfo("");
			entity.setIssuesTime(new Date());
		}
	}

}
