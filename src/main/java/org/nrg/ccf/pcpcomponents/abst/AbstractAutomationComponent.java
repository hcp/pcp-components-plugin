package org.nrg.ccf.pcpcomponents.abst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.automation.entities.Script;
import org.nrg.automation.entities.ScriptOutput;
import org.nrg.automation.services.ScriptRunnerService;
import org.nrg.automation.services.ScriptService;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PcpStatusEntityI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcp.inter.PipelineSelectorI;
import org.nrg.ccf.pcp.inter.PipelineStatusUpdaterI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.utils.WorkflowUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class AbstractAutomationComponent implements ConfigurableComponentI {
	
	private static final Logger _logger = LoggerFactory.getLogger(AbstractAutomationComponent.class);
	private List<String> _configYaml = null;
	private static ScriptService _scriptService = null;
	private static ScriptRunnerService _scriptRunnerService = null;
	protected final Gson _gson = new Gson();
	protected static ConfigService _configService = null;
	protected final static String AUTOMATION_TYPE = "automation";
	protected final static String WORKFLOW_TYPE = "workflow";
	protected final static String PARAMETERS_TYPE = "parameters";
	protected final static String ON_SUBMIT_TYPE = "submit";
	protected final static String SCRIPT_RETURN_TYPE = "return";
	protected final String _className = this.getClass().getName().replace(".", "");

	@Override
	public List<String> getConfigurationYaml() {
		if (_configYaml != null) {
			return _configYaml;
		}
		_configYaml = new ArrayList<>();
		final StringBuilder sb = new StringBuilder(_className)
				.append("AutomationScript:\n")
				.append("    id: " + _className + "-automation-script\n")
				.append("    name: " + _className + "-automation-script\n")
				.append("    kind: panel.select.single\n")
				.append("    label: ")
				.append(getLabelForClass(AUTOMATION_TYPE))
				.append("    options:\n");
		for (final Script script : getScripts()) {
			sb.append("        ").append(script.getScriptId()).append(": ").append(script.getScriptId()).append("\n");
		}
		sb.append(_className)
				.append("CreateWorkflow:\n")
				.append("    id: " + _className + "-create-workflow\n")
				.append("    name: " + _className + "-create-workflow\n")
				.append("    kind: panel.input.switchbox\n")
				.append("    label: ")
				.append(getLabelForClass(WORKFLOW_TYPE))
				.append("    value: false\n")
				.append("    onText: Yes\n")
				.append("    offText: No\n");
		_configYaml.add(sb.toString());
		return _configYaml;
	}
	
	protected String getLabelForClass(final String type) {
		String rt = "";
		if (type.equals(AUTOMATION_TYPE)) {
			rt = "Automation Script:\n"; 
		} else if (type.equals(WORKFLOW_TYPE)) {
			rt = "Create Workflow?:\n";
		} else if (type.equals(PARAMETERS_TYPE)) {
			rt = "Parameters YAML:\n";
		} else if (type.equals(ON_SUBMIT_TYPE)) {
			rt = "On Submit Status:\n";
		} else if (type.equals(SCRIPT_RETURN_TYPE)) {
			rt = "Script Return Status:\n";
		}
		if (this instanceof PipelineSelectorI) {
			return "Selector " + rt;
		} else if (this instanceof PipelineSubmitterI) {
			return "Submitter " + rt;
		} else if (this instanceof PipelineStatusUpdaterI) {
			return "Status Updater " + rt;
		} else if (this instanceof PipelinePrereqCheckerI) {
			return "Prereq Checker " + rt;
		} else if (this instanceof PipelineValidatorI) {
			return "Validator " + rt;
		}
		return rt;
	}
	
	private String getEventId() {
		if (this instanceof PipelineSelectorI) {
			return "PcpSelectorRun";
		} else if (this instanceof PipelineSubmitterI) {
			return "PcpSubmitterRun";
		} else if (this instanceof PipelineStatusUpdaterI) {
			return "PcpStatusUpdaterRun";
		} else if (this instanceof PipelinePrereqCheckerI) {
			return "PcpPrereqCheckerRun";
		} else if (this instanceof PipelineValidatorI) {
			return "PcpValidatorRun";
		}
		return "PcpRun";
	}
	
	public boolean execAutomationScript(String projectId, String pipelineId, UserI user) throws PcpAutomationScriptExecutionException {
		final Boolean createWorkflow = workflowDesired(projectId, pipelineId);
		return execAutomationScript(projectId, pipelineId, user, createWorkflow);
	}

	private boolean execAutomationScript(String projectId, String pipelineId, UserI user, boolean createWorkflow) throws PcpAutomationScriptExecutionException {
		_logger.info("Executing automation script -" + this.getClass().getName());
		final Map<String, Object> parameters = new HashMap<>();
		final String scriptId = getScriptId(projectId, pipelineId);
		parameters.put("user",  user);
		parameters.put("scriptId",  scriptId);
		parameters.put("dataType", XnatProjectdata.SCHEMA_ELEMENT_NAME);
		parameters.put("dataId", projectId);
		parameters.put("externalId", projectId);
		parameters.put("srcEventId", getEventId());
		parameters.put("srcEventClass", this.getClass().getName());
		if (createWorkflow) {
			generateWorkflowAndUpdateParameters(parameters, user);
		}
		final ScriptOutput scriptOut = execScript(scriptId, parameters);
		_logger.info("Executed automation script -" + this.getClass().getName() + "\n" + scriptOut);
		return (scriptOut != null);
	}
	
	public boolean execAutomationScript(PcpStatusEntityI entity, UserI user) throws PcpAutomationScriptExecutionException {
		final Boolean createWorkflow = workflowDesired(entity.getProject(), entity.getPipeline());
		return execAutomationScript(entity, user, null, createWorkflow);
	}
	
	public boolean execAutomationScript(PcpStatusEntityI entity, UserI user, Map<String,String> parameterMap) throws PcpAutomationScriptExecutionException {
		final Boolean createWorkflow = workflowDesired(entity.getProject(), entity.getPipeline());
		return execAutomationScript(entity, user, parameterMap, createWorkflow);
	}

	private boolean execAutomationScript(PcpStatusEntityI entity, UserI user, Map<String, String> parameterMap, boolean createWorkflow) throws PcpAutomationScriptExecutionException {
		_logger.info("Executing automation script -" + this.getClass().getName());
		final String scriptId = getScriptId(entity.getProject(), entity.getPipeline());
		final Map<String, Object> parameters = new HashMap<>();
		parameters.put("user", user);
		parameters.put("scriptId",  scriptId);
		parameters.put("dataType", entity.getEntityType());
		parameters.put("dataId", entity.getEntityId());
		parameters.put("externalId", entity.getProject());
		parameters.put("srcEventId", getEventId());
		parameters.put("srcEventClass", this.getClass().getName());
		parameters.put("statusEntity", entity);
		HashMap<String, String> argMap = new HashMap<>();
		argMap.put("statusEntity", _gson.toJson(entity).toString());
		if (parameterMap != null) {
			argMap.putAll(parameterMap);
		}
		parameters.put("arguments",_gson.toJson(argMap).toString());
		if (parameterMap != null) {
			for (final String key : parameterMap.keySet()) {
				if (!parameters.containsKey(key)) {
					parameters.put(key, parameterMap.get(key));
				}
			}
		}
		PersistentWorkflowI workflow = null;
		if (createWorkflow) {
			workflow = generateWorkflowAndUpdateParameters(parameters, user);
		}
		ScriptOutput scriptOut = null;
		boolean completed = false;
		try {
			scriptOut = execScript(scriptId, parameters);
			completed = true;
			_logger.info("Executed automation script -" + this.getClass().getName() + "\n" + scriptOut);
		} finally {
			if (workflow != null) {
				try {
					if (completed) {
						WorkflowUtils.complete(workflow, workflow.buildEvent());
					} else {
						WorkflowUtils.fail(workflow, workflow.buildEvent());
					}
				} catch (Exception e) {
					_logger.error("Couldn't update workflow status - " + workflow.getId());
				}
			}
		}
		return (scriptOut != null);
	}
	
	private PersistentWorkflowI generateWorkflowAndUpdateParameters(Map<String, Object> parameters, UserI user) {
		boolean runAsStandardScript = false;
		if (parameters.containsKey("arguments")) {
			try {
				Map<String, String> argMap = _gson.fromJson(parameters.get("arguments").toString(), new TypeToken<Map<String, String>>(){}.getType());
				runAsStandardScript = (argMap.containsKey("runAsStandardScript") && 
						argMap.get("runAsStandardScript").equalsIgnoreCase("TRUE"));
			} catch (Exception e) {
				// Do nothing
			}
		}
		final String action = "Executed " + ((runAsStandardScript) ? "" : "PCP automation ") +
			"script " + parameters.get("scriptId");
		final String justification = "PCP automation component launch";
		final String comment = "Executed script " + parameters.get("scriptId") + " triggered by " + parameters.get("srcEventClass");
        PersistentWorkflowI scriptWrk;
		try {
			scriptWrk = PersistentWorkflowUtils.buildOpenWorkflow(user, parameters.get("dataType").toString(), 
					parameters.get("dataId").toString(),
					parameters.get("externalId").toString(),
			        EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.PROCESS, action, justification, comment));
			assert scriptWrk != null;
			scriptWrk.setStatus(PersistentWorkflowUtils.RUNNING);
			WorkflowUtils.save(scriptWrk, scriptWrk.buildEvent());
			parameters.put("srcWorkflowId", scriptWrk.getWorkflowId());
			parameters.put("workflow", scriptWrk);
			return scriptWrk;
		} catch (Exception e) {
			_logger.error("PCP automation script execution:  Couldn't create and save workflow", e);
		}
		return null;
	}

	protected Boolean workflowDesired(String project, String pipeline) throws PcpAutomationScriptExecutionException {
		final String configValue = PcpComponentUtils.getConfigValue(project, pipeline, this.getClass().getName(),
				"-create-workflow", false).toString();
		return (configValue!=null) ? Boolean.valueOf(configValue) : false;
	}
	
	protected String getScriptId(String project, String pipeline) throws PcpAutomationScriptExecutionException {
		final String configValue = PcpComponentUtils.getConfigValue(project, pipeline, this.getClass().getName(),
				"-automation-script", true).toString();
		return configValue;
	}

	private ScriptOutput execScript(final String scriptId, final Map<String, Object> parameters) throws PcpAutomationScriptExecutionException {
		final Script script = getScriptService().getByScriptId(scriptId);
		if (script==null) {
			throw new PcpAutomationScriptExecutionException("ERROR:  No such automation script (" + scriptId + ")");
		}
		try {
			return getScriptRunner().runScript(script, parameters);
		} catch (NrgServiceException e) {
			throw new PcpAutomationScriptExecutionException(e);
		}
		
	}
	
	public List<Script> getScripts() {
		return getScriptService().getAll();
	}
	
	public ScriptService getScriptService() {
		if (_scriptService  == null) {
			_scriptService = XDAT.getContextService().getBean(ScriptService.class);
		}
		return _scriptService;
	}
	
	public ScriptRunnerService getScriptRunner() {
		if (_scriptRunnerService  == null) {
			_scriptRunnerService = XDAT.getContextService().getBean(ScriptRunnerService.class);
		}
		return _scriptRunnerService;
	}

}
