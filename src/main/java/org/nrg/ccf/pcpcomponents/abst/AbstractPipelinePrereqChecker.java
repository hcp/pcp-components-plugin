package org.nrg.ccf.pcpcomponents.abst;

import java.util.Date;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;

public abstract class AbstractPipelinePrereqChecker implements PipelinePrereqCheckerI {
	
	public void prereqsNotMet(PcpStatusEntity entity, String prereqsInfo) {
		if (entity.getPrereqs() == null || !prereqsInfo.equals(entity.getPrereqsInfo()) || entity.getPrereqs())  {
			entity.setPrereqs(false);
			entity.setPrereqsInfo(prereqsInfo);
			entity.setPrereqsTime(new Date());
		}
	}
	
	public void prereqsNotMet(PcpStatusEntity entity) {
		if (entity.getPrereqs() == null || !entity.getPrereqsInfo().isEmpty() || entity.getPrereqs())  {
			entity.setPrereqs(false);
			entity.setPrereqsInfo("");
			entity.setPrereqsTime(new Date());
		}
	}
	
	public void prereqsMet(PcpStatusEntity entity, String prereqsInfo) {
		if (entity.getPrereqs() == null || !prereqsInfo.equals(entity.getPrereqsInfo()) || !entity.getPrereqs())  {
			entity.setPrereqs(true);
			entity.setPrereqsInfo(prereqsInfo);
			entity.setPrereqsTime(new Date());
		}
	}
	
	public void prereqsMet(PcpStatusEntity entity) {
		if (entity.getPrereqs() == null || !entity.getPrereqsInfo().isEmpty() || !entity.getPrereqs())  {
			entity.setPrereqs(true);
			entity.setPrereqsInfo("");
			entity.setPrereqsTime(new Date());
		}
	}
	
}
