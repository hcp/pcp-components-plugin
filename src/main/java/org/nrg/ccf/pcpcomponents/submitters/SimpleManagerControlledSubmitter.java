package org.nrg.ccf.pcpcomponents.submitters;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

@PipelineSubmitter
public class SimpleManagerControlledSubmitter implements PipelineSubmitterI {

	@Override
	public List<String> getParametersYaml(String arg0, String arg1) {
		return ImmutableList.of();
	}

	@Override
	public boolean submitJob(final PcpStatusEntity entity, final PcpStatusEntityService entityService, final PipelineValidatorI validator,
			final Map<String, String> parameters, final UserI user) throws PcpSubmitException {
		// The exec manager will do the work of submission
		return true;
	}

}
