package org.nrg.ccf.pcpcomponents.submitters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcpcomponents.abst.AbstractAutomationComponent;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelineSubmitter
public class AutomationSubmitter extends AbstractAutomationComponent implements PipelineSubmitterI {
	
	private static final Logger _logger = LoggerFactory.getLogger(AbstractAutomationComponent.class);
	private List<String> _configYaml = null;

	@Override
	public List<String> getParametersYaml(String projectId, String pipelineId) {
		final List<String> parametersYaml = new ArrayList<>();
		try {
			final String configValue = PcpComponentUtils.getConfigValue(projectId, pipelineId,
					this.getClass().getName(), "-parameters-yaml", false).toString();
			final String configYaml = (configValue!=null) ? configValue : "";
			if (configYaml.length()>0) {
				parametersYaml.add(configYaml);
			}
		} catch (PcpAutomationScriptExecutionException e) {
			_logger.warn("WARNING:  Could not get parameters yaml for automation submitter - " + e.toString());
		}
		return parametersYaml;
	}
	
	@Override
	public List<String> getConfigurationYaml() {
		if (_configYaml != null) {
			return _configYaml;
		}
		_configYaml = super.getConfigurationYaml();
		final StringBuilder sb = new StringBuilder(_className)
				.append("SubmitStatus:\n")
				.append("    id: " + _className + "-on-submit-status\n")
				.append("    name: " + _className + "-on-submit-status\n")
				.append("    kind: panel.select.single\n")
				.append("    value: RUNNING\n")
				.append("    label: ")
				.append(getLabelForClass(ON_SUBMIT_TYPE))
				.append("    options:\n")
				.append("        'SUBMITTED': 'Submitted'\n")
				.append("        'QUEUED': 'Queued'\n")
				.append("        'RUNNING': 'Running'\n");
		sb.append(_className)
				.append("ScriptReturnStatus:\n")
				.append("    id: " + _className + "-script-return-status\n")
				.append("    name: " + _className + "-script-return-status\n")
				.append("    kind: panel.select.single\n")
				.append("    value: COMPLETE\n")
				.append("    label: ")
				.append(getLabelForClass(SCRIPT_RETURN_TYPE))
				.append("    options:\n")
				.append("        'NO_STATUS_CHANGE': 'NoStatusChange'\n")
				.append("        'QUEUED': 'Queued'\n")
				.append("        'COMPLETE': 'Complete'\n");
		sb.append(_className)
				.append("ParametersYaml:\n")
				.append("    id: " + _className + "-parameters-yaml\n")
				.append("    name: " + _className + "-parameters-yaml\n")
				.append("    kind: panel.textarea\n")
				.append("    code: yaml\n")
				/*  NOTE:  Couldn't get value to work with textarea here with or without yaml in the value 
				.append("    value: " + 
							"'runAsStandardScript:\\n    id:  runAsStandardScript\\n    kind:  panel.select.single\\n    label:  Run as Standard Automation Script?\\n    value:  false\\n    description:  This option issues a standard workflow rather than a PCP-specific workflow entry.  This may be useful for triggering later pipelines.\\n    options:\\n        true: true\\n        false: false\\n'"
						+ "\n")
				*/
				.append("    label: ")
				.append(getLabelForClass(PARAMETERS_TYPE));
		_configYaml.add(sb.toString());
		return _configYaml;
	}
	
	protected String getConfiguredOnSubmitStatus(String project, String pipeline) throws PcpAutomationScriptExecutionException {
		final String configValue = PcpComponentUtils.getConfigValue(project, pipeline, this.getClass().getName(),
				"-on-submit-status", false).toString();
		return (configValue!=null) ? configValue : "SUBMITTED";
	}
	
	protected String getConfiguredScriptReturnStatus(String project, String pipeline) throws PcpAutomationScriptExecutionException {
		final String configValue = PcpComponentUtils.getConfigValue(project, pipeline, this.getClass().getName(),
				"-script-return-status", false).toString();
		return (configValue!=null) ? configValue : "NO_STATUS_CHANGE";
	}

	@Override
	public boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator, Map<String, String> parameters,
			UserI user) throws PcpSubmitException {
		final String projectId = entity.getProject();
		final String pipelineId = entity.getPipeline();
		try {
			entity.setStatus(getConfiguredOnSubmitStatus(projectId, pipelineId));
			entity.setStatusTime(new Date());
			entityService.update(entity);
			final boolean rc = execAutomationScript(entity, user, parameters);
			final String configuredReturnStatus = getConfiguredScriptReturnStatus(projectId, pipelineId);
			if (configuredReturnStatus != null && !(configuredReturnStatus.equals("NO_STATUS_CHANGE"))) {
				entity.setStatus(configuredReturnStatus);
				entity.setStatusTime(new Date());
				entityService.update(entity);
			}
			return rc;
		} catch (PcpAutomationScriptExecutionException e) {
			final String errMsg = "EXCEPTION:  Script execution error for entity " + entity.toString() + " - " + e.toString();
			entity.setStatus(PcpConstants.PcpStatus.ERROR);
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(new Date());
			entityService.update(entity);
			throw new PcpSubmitException(e);
		}
	}

}
