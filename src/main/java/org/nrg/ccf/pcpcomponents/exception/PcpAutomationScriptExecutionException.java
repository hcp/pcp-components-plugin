package org.nrg.ccf.pcpcomponents.exception;

public class PcpAutomationScriptExecutionException extends Exception {
	
	private static final long serialVersionUID = -571098203514803427L;

	public PcpAutomationScriptExecutionException() {
		super();
	}

	public PcpAutomationScriptExecutionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PcpAutomationScriptExecutionException(String message, Throwable cause) {
		super(message, cause);
	}

	public PcpAutomationScriptExecutionException(String message) {
		super(message);
	}

	public PcpAutomationScriptExecutionException(Throwable cause) {
		super(cause);
	}

}
