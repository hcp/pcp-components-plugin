package org.nrg.ccf.pcpcomponents.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "pcpComponentsPlugin",
			name = "PCP Components Plugin"
		)
@ComponentScan({ 
		"org.nrg.ccf.pcpcomponents.conf",
		"org.nrg.ccf.pcpcomponents.components"
	})
public class PcpComponentsPlugin {
	
	public static Logger logger = Logger.getLogger(PcpComponentsPlugin.class);

	public PcpComponentsPlugin() {
		logger.info("Configuring the PCP Components Plugin.");
	}
	
}
