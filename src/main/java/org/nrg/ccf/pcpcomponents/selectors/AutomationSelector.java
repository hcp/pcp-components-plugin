package org.nrg.ccf.pcpcomponents.selectors;

import java.util.List;

import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineSelectorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcpcomponents.abst.AbstractAutomationComponent;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelineSelector
public class AutomationSelector extends AbstractAutomationComponent implements PipelineSelectorI {
	
	private static final Logger _logger = LoggerFactory.getLogger(AutomationSelector.class);

	@Override
	public List<PcpStatusEntity> getStatusEntities(PcpStatusEntityService _statusEntityService, String projectId,
			String pipelineId, UserI user) {
		try {
			execAutomationScript(projectId, pipelineId, user);
			return _statusEntityService.getProjectPipelineStatus(projectId, pipelineId);
		} catch (PcpAutomationScriptExecutionException e) {
			// TODO:  Need method to rethrow exception?
			_logger.error("Get status entities call failed");
		}
		return null;
	}

}
