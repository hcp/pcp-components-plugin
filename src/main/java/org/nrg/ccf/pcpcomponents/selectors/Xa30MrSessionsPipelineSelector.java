package org.nrg.ccf.pcpcomponents.selectors;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelineSelectorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.services.PcpStatusUpdateService;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelineSelector
public class Xa30MrSessionsPipelineSelector implements PipelineSelectorI,ConfigurableComponentI {
	
	public static final String DEFAULT_GROUP = "ALL";
	private static final Logger _logger = LoggerFactory.getLogger(Xa30MrSessionsPipelineSelector.class);
	protected final String _className = this.getClass().getName().replace(".", "");
	final RuntimeMXBean _mxBean = ManagementFactory.getRuntimeMXBean();
	final PcpStatusUpdateService _statusUpdateService = XDAT.getContextService().getBean(PcpStatusUpdateService.class);

	@Override
	public List<String> getConfigurationYaml() {
		final List<String> returnList = new ArrayList<>();
		return returnList;
	}

	@Override
	public List<PcpStatusEntity> getStatusEntities(PcpStatusEntityService _statusEntityService, String projectId, String pipelineId,
			UserI user) {
		final List<PcpStatusEntity> returnList = new ArrayList<>();
		final List<PcpStatusEntity> unmatchedEntities;
		try {
			unmatchedEntities = _statusEntityService.getProjectPipelineStatus(projectId, pipelineId);
			Collections.sort(unmatchedEntities);
		} catch (Throwable t) {
			_logger.error("Exception thrown accessing status entity service", t);
			return null;
		}
        final CriteriaCollection cc = new CriteriaCollection("OR");
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", projectId);
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", projectId);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, false);
		outerLoop:
		for (final XnatMrsessiondata exp : exps) {
			final Iterator<PcpStatusEntity> i = unmatchedEntities.iterator();
			boolean foundMatch = false;
			while (i.hasNext()) {
				final PcpStatusEntity entity = i.next();
				if (entity.getEntityId().equals(exp.getId()) || (entity.getEntityLabel().equals(exp.getLabel()))) {
					returnList.add(entity);
					i.remove();
					foundMatch = true;
					final String subGroup = getXa30Group(exp);
					if (!entity.getSubGroup().equals(subGroup)) {
						entity.setSubGroup(subGroup);
						_statusEntityService.update(entity);
					}
					if (entity.getEntityId().equals(exp.getId())) {
						if (!entity.getEntityLabel().equals(exp.getLabel())) {
							_logger.warn("WARNING:  Experiment Label has changed for session " + exp.getId() +
									" - updating StatusEntityID (OLDLABEL=" + entity.getEntityLabel() +  ", NEWLABEL=" + exp.getLabel() + ")");
							entity.setEntityLabel(exp.getLabel());
							_statusEntityService.update(entity);
						}
						if (entity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
							entity.setStatus(PcpConstants.PcpStatus.UNKNOWN);
							entity.setStatusTime(new Date());
							_statusEntityService.update(entity);
						}
					} else {
						_logger.warn("WARNING:  Experiment ID has changed for session " + exp.getLabel() +
								" - updating StatusEntityID (OLDID=" + entity.getId() +  ", NEWID=" + exp.getId() + ")");
						entity.setEntityId(exp.getId());
						if (entity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
							entity.setStatus(PcpConstants.PcpStatus.UNKNOWN);
							entity.setStatusTime(new Date());
						}
						_statusEntityService.update(entity);
					}
				}
				if (foundMatch) {
					// We know we're done once we've found a match and are no longer matching, because we've sorted the list
					continue outerLoop;
				}
			}
			try {
				final PcpStatusEntity newEntity = new PcpStatusEntity();
				newEntity.setProject(projectId);
				newEntity.setPipeline(pipelineId);
				newEntity.setEntityType(XnatMrsessiondata.SCHEMA_ELEMENT_NAME);
				newEntity.setEntityId(exp.getId());
				newEntity.setEntityLabel(exp.getIdentifier(projectId));
				newEntity.setStatus(PcpConstants.PcpStatus.NOT_SUBMITTED);
				newEntity.setStatusTime(new Date());
				final String subGroup = getXa30Group(exp);
				if (subGroup != null) {
					newEntity.setSubGroup(subGroup);
					final PcpStatusEntity createdEntity = _statusEntityService.create(newEntity);
					returnList.add(createdEntity);
					_statusUpdateService.refreshStatusEntityCheckAndValidate(createdEntity, user, false);
				} else {
					_logger.error("ERROR:  getSubgroupByLabel returned null for entity.  Entity could not be added.  " +
							"  (entity=" + newEntity.toString() + ")");
				}
			} catch (Exception e) {
				// Could be an overlooked constraint violation or something.  We need to continue and try other entities.
				_logger.error("ERROR:  Exception thrown adding status entity", e);
			}
			
		}
		//
		// Update status for sessions that no longer exist.
		//
		// CCF-326:  For some reason, this code was often executing at Tomcat startup.  It seems like this check 
		// may be executing before the XnatMrsessiondatas.getXnatMrsessiondatasByField method is able to 
		// return values.  Let's just skip this code near Tomcat startup to prevent it messing up status when 
		// the run is unable to access the session list.  NOTE:  This fix is probably redundant, because 
		// there's a similar check in PcpStatusUpdate (the scheduled task), but just in case....
		final Long upTime = (_mxBean!=null) ? _mxBean.getUptime() : Long.MAX_VALUE;
		if (upTime>PcpConfigConstants.UP_TIME_WAIT && exps.size()>0) {
			for (PcpStatusEntity statusEntity : unmatchedEntities) {
				if (statusEntity.getStatus() != null && statusEntity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
					continue;
				}
				statusEntity.setStatus(PcpConstants.PcpStatus.REMOVED);
				statusEntity.setStatusTime(new Date());
				_statusEntityService.update(statusEntity);
			}
		} else {
			_logger.debug("Skipping step to set status to REMOVED due to insufficient Tomcat uptime or empty experiment list.  (UPTIME=" +
					upTime + "ms, UNMATCHED_ENTITIES=" + unmatchedEntities.size() + ")");
		}
		return returnList;
	}
	
	private String getXa30Group(XnatImagesessiondata exp) {
		return (ExperimentUtils.isXA30Session(exp)) ? "XA30" : "VE11";
	}

}
