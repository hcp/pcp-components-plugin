package org.nrg.ccf.pcpcomponents.prereqcheckers;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcpcomponents.abst.AbstractAutomationComponent;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PipelinePrereqChecker
public class AutomationPrereqChecker extends AbstractAutomationComponent implements PipelinePrereqCheckerI {
	
	private static final Logger _logger = LoggerFactory.getLogger(AutomationPrereqChecker.class);
	private final PcpStatusEntityService _statusEntityService = XDAT.getContextService().getBean(PcpStatusEntityService.class);

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		try {
			execAutomationScript(statusEntity, user);
		} catch (PcpAutomationScriptExecutionException e) {
			// TODO:  Need method to rethrow exception?
			_logger.error("Prereq checker call failed");
		}
		// The statusEntity was likely updated in the automation script.  We need to refresh it.
		if (_statusEntityService!=null) {
			_statusEntityService.refresh(statusEntity);
		} else {
			_logger.error("ERROR:  Could not retrieve PcpStatusEntityService");
		}
	}

}
