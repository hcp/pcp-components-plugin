package org.nrg.ccf.pcpcomponents.prereqcheckers;

import java.util.Date;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.xft.security.UserI;

/**
 * The Class SimpleNoPrereqPrereqChecker.
 * 
 * This PrereqChecker can be used when there are basically no prerequisites (i.e. if the entity exists, the prereqs are met).
 * 
 */
@PipelinePrereqChecker
public class SimpleNoPrereqPrereqChecker implements PipelinePrereqCheckerI {

	/* (non-Javadoc)
	 * @see org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI#checkPrereqs(org.nrg.ccf.pcp.entities.PcpStatusEntity, org.nrg.xft.security.UserI)
	 */
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		final Boolean prereqs = statusEntity.getPrereqs();
		if (prereqs == null || !prereqs) {
			statusEntity.setPrereqs(true);
			statusEntity.setPrereqsInfo("");
			statusEntity.setPrereqsTime(new Date());
		}
		return;
	}

}
