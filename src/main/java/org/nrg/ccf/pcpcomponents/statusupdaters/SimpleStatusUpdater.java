package org.nrg.ccf.pcpcomponents.statusupdaters;

import java.util.Date;

import org.nrg.ccf.pcp.abst.AbstractStatusUpdater;
import org.nrg.ccf.pcp.anno.PipelineStatusUpdater;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineStatusUpdaterI;
import org.nrg.xft.security.UserI;

@PipelineStatusUpdater
public class SimpleStatusUpdater extends AbstractStatusUpdater implements PipelineStatusUpdaterI {

	@Override
	public void updateStatus(PcpStatusEntity statusEntity, UserI user) {
		super.updateStatus(statusEntity, user);
		if (isSubmittedOrRunningStatus(statusEntity.getStatus())) {
			// Do nothing here.  The ExecManager should manage the status.
		} else if (statusEntity.getStatus()==null || statusEntity.getStatus().equals("")) {
			statusEntity.setStatus(PcpConstants.PcpStatus.NOT_SUBMITTED);
			statusEntity.setStatusTime(new Date());
		}
	}
	
}
